import json

from django.http import HttpResponse, HttpResponseNotAllowed, HttpResponseBadRequest
from django.core.paginator import Paginator, EmptyPage
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods

from models import Task, ValidationError
from decorators import json_request_body

def _validate_and_save_task_from_api(task, data):
    try:
        task.update_from_api(data)
    except ValidationError as e:
        return HttpResponseBadRequest(str(e))

    task.save()
    return HttpResponse()

def _tasks_get(request):
    page = int(request.GET.get('page', 1))
    sort_column = request.GET.get('sort_column', 'id')
    sort_order = request.GET.get('sort_order', 'asc')

    if (sort_column not in ['id', 'description', 'date_start'] or
            sort_order not in ['asc', 'desc']):
        return HttpResponseBadRequest("Invalid sort specification")

    q = Task.objects.all().order_by(sort_column)
    if sort_order == 'desc':
        q = q.reverse()
    paginator = Paginator(q, settings.TASKS_PER_PAGE)

    try:
        tasks = paginator.page(page)
    except EmptyPage:
        # If requested page with number larger than number of pages, return last page
        page = paginator.num_pages
        tasks = paginator.page(page)

    result = {
        'tasks': [task.to_api() for task in tasks.object_list],
        'page': page,
        'pages': paginator.num_pages,
    }

    return HttpResponse(json.dumps(result), content_type="application/json")

@json_request_body
def _tasks_put(request):
    task = Task()
    return _validate_and_save_task_from_api(task, request.data)

@json_request_body
def _task_post(request, id):
    task = get_object_or_404(Task, pk=id)
    return _validate_and_save_task_from_api(task, request.data)

def _task_delete(request, id):
    get_object_or_404(Task, pk=id).delete()
    return HttpResponse()

@require_http_methods(["GET", "PUT"])
def tasks(request):
    """
    Tasks view: GET returns a list of tasks, paged and sorted, PUT creates a new task
    """
    if request.method == "GET":
        return _tasks_get(request)
    elif request.method == "PUT":
        return _tasks_put(request)

@require_http_methods(["POST", "DELETE"])
def task(request, id):
    """
    Single task view: POST modifies task, DELETE deletes it
    """
    if request.method == "POST":
        return _task_post(request, id)
    elif request.method == "DELETE":    
        return _task_delete(request, id)

