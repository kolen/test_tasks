import json

def json_request_body(func):
    """
    Decorator for views that parses JSON request body and puts parsed data to request.data.
    In case of invalid json returns HttpResponseBadRequest.
    """
    def decorated(request, *args, **kwargs):
        try:
            request.data = json.loads(request.body)
        except ValueError:
            return HttpResponseBadRequest("Invalid json")

        return func(request, *args, **kwargs)
    return decorated
