angular.module('tasks', ['ngResource']).
  controller('TasksCtrl', ['$scope', '$resource', function($scope, $resource) {
    /// State
    var isoformat = 'YYYY-MM-DDTHH:mm:ss'

    $scope.tasks_api = $resource('/tasks', null, {
      query: {method: 'GET'},
      create: {method: 'PUT'}
    })
    $scope.task_api = $resource('/tasks/:id', {id: '@id'})

    $scope.tasks = [] // page of tasks loaded into client
    $scope.loading = false // if loading operation is running
    $scope.selected_task_id = null
    $scope.table_headers = [
      ["id", "Id", "col-xs-1"],
      ["description", "Description", "col-xs-7 col-sm-9"],
      ["date_start", "Start date", "col-xs-4 col-sm-2"]
    ]
    $scope.sort = {
      column: 'id',
      descending: false
    }
    $scope.page = 1 // active page
    $scope.num_pages = 1 // number of pages available

    $scope.edited = {
      id: null, // 0 if creating new task
      description: null,
      date_start: null,
      saving: false
    }

    /// Calculated properties

    // CSS class of table column
    $scope.table_column_class = function(column, added) {
      return (column == $scope.sort.column && 'sort-' + ($scope.sort.descending ? 'desc' : 'asc')) + " " + added
    }
    // List of page numbers ([1, 2, 3, 4, ...])
    $scope.pages = function() {
      return _.range(1, $scope.num_pages+1)
    }
    // If selected task is in set of loaded tasks (on loaded page)
    // Selected task id persists even when sort order is changed or page is
    // switched.
    $scope.is_selected_task_in_loaded_tasks = function() {
      return $scope.selected_task_id &&
        _.contains(_.map($scope.tasks, function(t) {return t.id}),
          $scope.selected_task_id)
    }

    /// Methods

    $scope.selectPage = function(p) {
      $scope.page = p
      $scope.reload()
    }
    // Select page current + offset, it ignores if it will be out of range
    $scope.selectPageRelative = function(offset) {
      var new_page = $scope.page + offset
      if (new_page <= $scope.num_pages && new_page >= 1) {
        $scope.selectPage(new_page)
      }
    }

    // Change sorting on column. First time it sets asc sorting on the column,
    // next time it changes sort order.
    $scope.changeSorting = function(column) {
      var sort = $scope.sort
      if (sort.column == column) {
        sort.descending = !sort.descending
      } else {
        sort.column = column
        sort.descending = false
      }
      $scope.reload()
    }

    $scope.selectTask = function(id) {
      $scope.selected_task_id = id
    }

    // Reload dataset of tasks (current page) from server
    $scope.reload = function() {
      $scope.loading = true
      $scope.tasks_api.query({
        page: $scope.page,
        sort_column: $scope.sort.column,
        sort_order: $scope.sort.descending ? 'desc' : 'asc'
      }, function(response) {
        $scope.tasks = response.tasks
        $scope.num_pages = response.pages
        $scope.page = response.page
        $scope.loading = false
      })
    }

    // Open editor for editing or adding new task. Id of 0 specifies
    // new task.
    $scope.openEditor = function(id) {
      $scope.edited.id = id

      if (id === 0) { // New task
        $scope.edited.description = ''
        $scope.edited.date_start = moment.utc()
      } else { // Existing task
        var task = _.find($scope.tasks, function(task) {return task.id == id})

        $scope.edited.description = task.description
        $scope.edited.date_start = moment.utc(task.date_start, isoformat)
      }
      $('#editor-modal').modal()
    }

    $scope.setDateNow = function() {
      $scope.edited.date_start = moment.utc()
    }

    // Save task
    $scope.save = function() {
      $scope.edited.saving = true
      var ed = $scope.edited
      var task_data = {
        description: ed.description,
        date_start: ed.date_start.utc().format(isoformat)
      }
      var on_success = function() {
        $scope.edited.saving = false
        $('#editor-modal').modal('hide')
        $scope.reload()
      }
      if (ed.id === 0) {
        $scope.tasks_api.create(task_data, on_success)
      } else {
        task_data.id = ed.id
        $scope.task_api.save(task_data, on_success)
      }
    }

    // Delete task
    $scope.delete = function(id) {
      $scope.task_api.delete({id: id}, function() {
        $scope.loading = true
        $scope.reload()
      })
    }

    $scope.reload()
  }]).

  directive('datetime', function() {
    var format = "YYYY-MM-DD HH:mm:ss"
    var pattern = /^\s*\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}\s*$/
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attr, ngModel) {
        ngModel.$parsers.push(function(text) {
          var date = moment(text, format)
          ngModel.$setValidity('dateInvalid', date.isValid() && text.match(pattern))
          return date
        })
        ngModel.$formatters.push(function(datetime_moment) {
          return datetime_moment && datetime_moment.local().format(format)
        })
      }
    };
  }).

  filter('datetime', function() {
    return function(input) {
      return moment.utc(input).local().format("YYYY-MM-DD HH:MM")
    }
  })
