from django.db import models
import datetime

class ValidationError(Exception):
    pass

class Task(models.Model):
    description = models.TextField()
    date_start = models.DateTimeField(db_index=True)

    def to_api(self):
        return {
            'id': self.id,
            'description': self.description,
            'date_start': self.date_start.isoformat(),
        }

    def update_from_api(self, data):
        try:
            date = datetime.datetime.strptime(data.get('date_start', ''), "%Y-%m-%dT%H:%M:%S")
        except ValueError:
            raise ValidationError("Invalid date")

        if data.get('description', '').strip() == '':
            raise ValidationError("Invalid description")

        self.description = data['description']
        self.date_start = date
