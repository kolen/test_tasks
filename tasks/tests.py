
import json
import math
import itertools
import datetime

from django.test import TestCase
from django.test.client import Client
from django.conf import settings

from models import Task

class TasksTest(TestCase):
    TASKS_IN_INITIAL_DATA = 52
    SORT_VARIANTS = itertools.product(['id', 'description', 'date_start'], [True, False])

    def test_get_tasks(self):
        c = Client()

        for sort_column, sort_reverse in self.SORT_VARIANTS:            
            num_pages = int(math.ceil(float(self.TASKS_IN_INITIAL_DATA) / settings.TASKS_PER_PAGE))
            all_tasks = []

            # Read all of the pages sequentially, adding each page to all_tasks
            for page in range(1, num_pages + 1):
                r = c.get('/tasks', {
                    'page': page,
                    'sort_column': sort_column,
                    'sort_order': 'desc' if sort_reverse else 'asc',
                })
                self.assertEqual(r.status_code, 200)
                data = json.loads(r.content)

                self.assertEquals(data['pages'], num_pages)
                self.assertEquals(data['page'], page)
                self.assertEquals(len(data['tasks']), settings.TASKS_PER_PAGE if page < num_pages 
                    else self.TASKS_IN_INITIAL_DATA % settings.TASKS_PER_PAGE)

                all_tasks += data['tasks']

            # Check that got all of records and each occured once
            all_ids = set(task['id'] for task in all_tasks)
            self.assertEquals(len(all_ids), self.TASKS_IN_INITIAL_DATA)
            self.assertEquals(len(all_tasks), self.TASKS_IN_INITIAL_DATA)

            # Check for correct sorting
            sorted_tasks = sorted(all_tasks, key=lambda t: t[sort_column], reverse=sort_reverse)
            self.assertEquals(all_tasks, sorted_tasks)

    def test_invalid_sort(self):
        c = Client()
        r = c.get('/tasks', {'sort_column': 'blah'})
        self.assertEquals(r.status_code, 400)
        r = c.get('/tasks', {'sort_column': 'id', 'sort_order': 'blah'})
        self.assertEquals(r.status_code, 400)

    def test_put_task(self):
        tasks_before = list(Task.objects.all())

        c = Client()
        r = c.put('/tasks', json.dumps({
            'description': 'Test Description Of Added Task',
            'date_start': '2013-09-12T17:06:55'
        }))
        self.assertEquals(r.status_code, 200)

        tasks_after = list(Task.objects.all())
        diff = set(tasks_after) - set(tasks_before)
        self.assertEquals(len(diff), 1)

        added_task = diff.pop()
        self.assertEquals(added_task.description, 'Test Description Of Added Task')
        self.assertEquals(added_task.date_start, datetime.datetime(2013, 9, 12, 17, 6, 55))

    def test_put_task_invalid_description(self):
        r = Client().put('/tasks', json.dumps({
            'description': '',
            'date_start': '2013-09-12T17:24:12',
            }), content_type='text/json')
        self.assertEquals(r.status_code, 400)

    def test_put_task_spacesonly_description(self):
        r = Client().put('/tasks', json.dumps({
            'description': '     ',
            'date_start': '2013-09-12T17:24:12',
            }), content_type='text/json')
        self.assertEquals(r.status_code, 400)

    def test_put_task_invalid_date(self):
        r = Client().put('/tasks', json.dumps({
            'description': 'Test descr',
            'date_start': 'sdcjsldkjcls',
            }), content_type='text/json')
        self.assertEquals(r.status_code, 400)

    def test_put_task_no_date(self):
        r = Client().put('/tasks', json.dumps({
            'description': 'Test descr',
            }), content_type='text/json')
        self.assertEquals(r.status_code, 400)

    def test_post_task(self):
        r = Client().post('/tasks/3', json.dumps({
            'description': 'New description 111',
            'date_start': '2013-09-13T17:39:32',
            }), content_type='text/json')
        self.assertEquals(r.status_code, 200)
        task = Task.objects.get(pk=3)
        self.assertEquals(task.description, "New description 111")
        self.assertEquals(task.date_start, datetime.datetime(2013, 9, 13, 17, 39, 32))

    def test_post_task_invalid(self):
        r = Client().post('/tasks/3', json.dumps({
            'description': '',
            'date_start': '2013-09-13T17:39:32',
            }), content_type='text/json')
        self.assertEquals(r.status_code, 400)

    def test_post_task_404(self):
        r = Client().post('/tasks/9999', json.dumps({
            'description': '',
            'date_start': '2013-08-13T17:39:37',
            }), content_type='text/json')
        self.assertEquals(r.status_code, 404)

    def test_delete_task(self):
        r = Client().delete('/tasks/12')
        self.assertEquals(r.status_code, 200)

        ids = set(task.id for task in Task.objects.all())
        expected_ids = set(range(1, self.TASKS_IN_INITIAL_DATA + 1)) - set([12])
        self.assertEquals(ids, expected_ids)

    def test_detele_task_404(self):
        r = Client().delete('/tasks/2873')
        self.assertEquals(r.status_code, 404)

    def test_invalid_methods(self):
        r = Client().delete('/tasks')
        self.assertEquals(r.status_code, 405)
        r = Client().post('/tasks')
        self.assertEquals(r.status_code, 405)
        r = Client().get('/tasks/23')
        self.assertEquals(r.status_code, 405)
        r = Client().put('/tasks/43')
        self.assertEquals(r.status_code, 405)

    def test_index_page(self):
        r = Client().get('/')
        self.assertEquals(r.status_code, 200)
